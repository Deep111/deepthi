
public class While_continue {

	public static void main(String[] args) {
		System.out.println("Print the numbers from 1 to 10");
		int n = 1;
		while (n <= 10) {
			if (n == 8) {
				break;
			}

			System.out.println(n);

			n++;

		}

	}

}
