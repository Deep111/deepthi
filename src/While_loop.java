
public class While_loop {

	public static void main(String[] args) {

		System.out.println("The first ten numbers:");
		int i = 1;
		while (i <= 10) {
			System.out.println(i);
			i++;
		}

	}

}
