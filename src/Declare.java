
public class Declare {

	public static void main(String[] args) {
		// Declare variables and display their values
		int a = 10;
		int b = 12;
		float c = 12.5f;
		//String s = "Java Programming";
		System.out.println("the value of a is " + a);
		System.out.println("the value of b is " + b);
		System.out.println("the value of c is " + c);
		//System.out.println("the value of s is " + s);
	}

}
