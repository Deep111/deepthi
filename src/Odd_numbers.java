
public class Odd_numbers {

	public static void main(String[] args) {
		System.out.println("The odd numbers from 1 to 19:");
		for (int i = 1; i <= 19; i++) {
			if (i % 2 != 0) {
				System.out.println(i);
			}

		}

	}

}
